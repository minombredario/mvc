
        <?php 
            include ("header.php") ;
            require ("../controlador/funciones.php");
            require ("../vista/Config.php");
            
            $vista = getVista();                     
            $modelo = getModelo();
            
        ?>
            
        <h1>Gestión de las Asignaturas</h1>
            <table width="50%" border="0" align="center">
                <tr >
                  <td class="primera_fila">Id</td>
                  <td class="primera_fila">Nombre</td>
                  <td class="primera_fila">Horas</td>
                  <td class="primera_fila">Profesor</td>
                  <td class="sin">&nbsp;</td>
                  <td class="sin">&nbsp;</td>
                  <td class="sin">&nbsp;</td>
                </tr> 

                    <?php foreach($modelo->readAsignatura() as $asignatura): ?>

                    <tr>
                        <td ><?php echo $asignatura->getId();?></td>
                        <td style="text-align: left"><?php echo $asignatura->getNombre();?></td>
                        <td><?php echo $asignatura->getHoras();?></td>
                        <td><?php echo $asignatura->getProfesor()->getNombre();?></td>
                   
                        <td class="bot"><a href="../controlador/accion.php?accion=delasig&id=<?php echo $asignatura->getId()?>"><input type='button' name='delasig' id='del' value='Borrar'></a></td>
                        <td class='bot'><a href="editarAsignatura.php?id=<?php echo $asignatura->getId()?>&nombre=<?php echo $asignatura->getNombre()?>&horas=<?php echo $asignatura->getHoras();?>&idp=<?php echo $asignatura->getProfesor()->getId();?>&nprof=<?php echo $asignatura->getProfesor()->getNombre();?>"><input type='button' name='up' id='up' value='Actualizar'></a></td>
                        
                    </tr>

                    <?php endforeach;?> 
                    <form action="../controlador/accion.php" method="GET">  
                <tr>
                    <td><input type='hidden' name='id' size='10' class='centrado' value="<?php echo $modelo->getId("asignaturas")?>" readonly="readonly" style="fon"></td>
                    <td><input type='text' name='nom' size='10' class='centrado' required></td>
                    <td><input type='text' name='hora' size='10' class='centrado' required></td>
                    <td>
                        <select name="prof" id="profesores">
                            <option value="defecto" selected="selected">Seleciona profesor</option>
                        <?php foreach($modelo->readProfesor() as $profesor): ?>
                            <option value="<?php echo $profesor->getId();?>"><?php echo $profesor->getNombre();?></option>
                        <?php endforeach; ?>       
                        </select>
                    </td>
                    <td class='bot'><input type='submit' name='crearasig' id='cr' value='Insertar'></td>
                   
                </tr>
            </form>
            </table>
        
        <div style="position: relative; margin-left: 60%">
            <?php if($vista=="Sql"):?>
                <a href='../controlador/ControladorSql.php'>Volver</a>
            <?php endif; if($vista=="Ficheros"):?>
                <a href='../controlador/ControladorFicheros.php'>Volver</a>
            <?php endif;?>  
        </div>
        <p>&nbsp;</p>
<?php include ("footer.php");?>