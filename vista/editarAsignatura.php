<?php 
    include ("header.php");
    require ("../controlador/funciones.php");
    include ("Config.php");
    
    $modelo = getModelo();
    
    //$profesores=$base->query("Select * from profesores")->fetchALL(PDO::FETCH_OBJ); 
    
    if(!isset($_POST["upasig"])){//si no se pulsa el boton bot_actualizar, coge estos datos
        $id = recoge('id');
        $nombre = recoge('nombre');
        $horas = recoge('horas');
        $Idprofesor = recoge ('idp');
        $Nprofesor = recoge ('nprof');
        $profesor2 = new Profesor($Idprofesor,$Nprofesor);
        
    }else{ //si los datos vienen del boton bot_actualizar usa estos, con metodo $_POST hace esto
        
        $id = recoge('id');
        $nombre = recoge('nombre');
        $horas = recoge('horas');
        $Idprofesor = recoge ('idp');
        
        
        header("Location:../controlador/accion.php?id=". $id ."&nombre=". $nombre."&horas=".$horas."&idp=".$Idprofesor."&accion=upasig");
    }
   
     
?>
        <h1>ACTUALIZAR</h1>

        <p>

        </p>
        <p>&nbsp;</p>
        <form name="form1" method="post" action="<?php echo $_SERVER['PHP_SELF']?>"><!--envio los datos a la misma pagina para hacer el insert-->
            <table width="25%" border="0" align="center">
                <tr>
                    <td></td>
                    <td><label for="id"></label>
                    <input type="hidden" name="id" id="id" value="<?php echo $id?>">
                </tr>   
                <tr>
                    <td>Nombre</td>
                    <td><label for="nom"></label>
                    <input type="text" name="nombre" id="nom" value="<?php echo $nombre ?>" required></td>
                </tr>
                <tr>
                    <td>Horas</td>
                    <td><label for="hora"></label>
                    <input type="text" name="horas" id="nom" value="<?php echo $horas ?>" required></td>
                </tr>
                <tr>
                    <td>Profesor</td>
                    <td><label for="prof"></label>
                    <select name="idp" id="profesores">
                            <option value="<?php echo $profesor2->getId()?>" selected="selected"><?php echo $profesor2->getNombre()?></option>
                    <?php foreach($modelo->readProfesor() as $profesor):?>
                            <option value="<?php echo $profesor->getId()?>"><?php echo $profesor->getNombre()?></option>
                    <?php endforeach; ?>

                    </select>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" name="upasig" id="bot_actualizar" value="Actualizar"></td>
                </tr>
            </table>
        </form>
        <p>&nbsp;</p>
        <div style="position: relative; margin-left: 60%">
            <a href="VistaAsignaturas.php"  >Volver</a>
        </div>
        <?php require ("footer.php");?>

